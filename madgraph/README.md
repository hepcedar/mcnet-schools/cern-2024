# MadGraph+Rivet+Contur Tutorial

## TUESDAY & WEDNESDAY

Contacts: Jon Butterworth, Martin Habedank, Ramon Winterhalder, Zenny Wettersten

Welcome to this tutorial, which will lead you through generating some BSM events with MadGraph, and then runnging them through
Rivet and Contur to see what current measurements tell you about them.

:information_source: Don't forget that you can always ask us for help -- but we have tried to provide resources for self-help in the text.

### Introduction

#### Before you start: Event store

You'll hopefully be generating some MadGraph events of your own, and we have some prepared ones too. We suggest making a working directory, cd into it,
and then make a subdirectory called `Events`. Put your HepMC files in here.

The prepared SM HepMC event files for you to analyse should be downloaded as well. You can
download and untar as follows. Go into your Events directory and execute:

```
   wget "https://rivetval.web.cern.ch/rivetval/TUTORIAL/truth-analysis.tar.gz" -O- | tar -xz --no-same-owner
```

You should see two files `Wjets13TeV_10k.hepmc.gz` and `Zjets13TeV_10k.hepmc.gz`, corresponding to particle-level $`W`$+jets and $`Z`$+jets events in
proton-proton collisons at a centre-of-mass energy of 13 TeV. Now `cd ..` to go back into your working directory.

If you already have Rivet yoda results you want to run contur on, then if they are somewhere below your working directory they will also be visible to
Rivet and Contur from within the docker image. We suggest making a `Rivet_Output` directory in your working directory and putting them there. (Note that
we are using Rivet 4 and thus Yoda 2 here; there may be problems running on files generated with Rivet 3 / Yoda 1.)

 
#### Download the docker image for this tutorial `hepstore/contur-tutorial:mcnet24` image. In a terminal, this is accomplished by:

```
docker pull hepstore/contur-tutorial:mcnet24
```
 
#### Run the container interactively, mounting your working directory.

```
docker container run -it -v $PWD:$PWD -w $PWD hepstore/contur-tutorial:mcnet24 /bin/bash
```

where the `-v $PWD:$PWD -w $PWD` ensures that you have access to your current directory also from within the container.
To check that it works, try e.g.

```
  rivet --version
```

which should return `rivet v4.0.0`. Then try 

```
  contur --version
```

The first time you run this, it will take a few seconds because there is some python set up behind the scenes. It should (eventually) return
`Contur 3.0.x`.

`ls Events` should show you all the hepmc event files you can run Rivet on. `ls Rivet_Ouput` should show you any yoda files
you had ready to run Contur on.

### Generating BSM Events with MadGraph 

For details on the MadGraph Tutorial see [MadGraph Slides](https://indico.cern.ch/event/1374994/sessions/552945/attachments/2873785/5036038/mg5_mcnet.pdf)

### Rivet tutorial

See [Rivet tutorial](../rivet/rivet-tutorial.pdf). You should skip/ignore the "Environmental Setup" section (you've already done that!) so go straight to Section 3, and
you can learn how to write your own Rivet routine, how to handle MC weights and uncertainties, and more.

### Contur Tutorial

If you downloaded the docker more than an hour beofre the Wednesday part of the tutorial, you need to either pull it again, or else
upgrade the installed matplotlib version, as it is a bit outdated in the docker image.

For this call
```
pip install --root-user-action=ignore --user matplotlib --upgrade
pip install --root-user-action=ignore --user numpy --upgrade
pip install --root-user-action=ignore --user scipy --upgrade
export PYTHONPATH=/root/.local/lib/python3.10/site-packages:$PYTHONPATH
```

Here are the [introductory slides](../contur/contur-mcnet.pptx).

The simplest first step is to work with a single YODA file of BSM events. The commands are simple:

        $ contur myrivetresults.yoda

- Run `contur --help` to see more options. 

- Normally, this YODA file contains histograms corresponding to a set of Rivet analyses. The histograms are filled with events from your favourite BSM theory.
By running `contur` on this YODA file, you check the compatibility of the BSM+SM theory with the results from experimental data.
(Strictly what we do is compare the likelihoods of BSM+SM vs Data and SM vs Data, to evaluate the CLs exclusion.)
In this first run, we focus on a *single* point of BSM parameter space. Later on, we will see how to generate 2D exclusion plots.

- As well as the files you have hopefully generated in the Madgraph tutorial, there are sample yoda files to try this on in the test area, under `tests/sources/myscan00`. Running (for example) `contur $CONTUR_DATA_PATH/tests/sources/myscan00/13TeV/0000/runpoint_0000.yoda.gz` will run on the results of the first BSM point.

- This will output an ANALYSIS folder and a log file, and print some other information to the screen. The ANALYSIS directory contains a small SQLite database holding the contur statistics, and some subdirectries with python script which will be used below to plot histograms.

### Looking at the Contur/Rivet histograms

- We can now look in more detail at the Contur results.

This uses the set of plotting scripts that are already generated and stored in `ANALYSIS/plots/`.
Have a browse through this folder and you will find that each histogram for all the analyses have an associated Python script.
By executing these Python scripts you generate plots showing the differential distributions with the data, SM prediction and SM+BSM prediction.

To do this, go back to your run area (the directory which contains your ANALYSIS directory) and show the level of exclusion for every histograms
from each analysis as follows
(in this example we are looking at the example 13TeV results for the point stored in the 13TeV/0010 directory, but you could replace that by your
own results):

        $ cd ..
        $ contur-rivetplots -p
        INFO - Contur version 3.0.x
        INFO - See https://hepcedar.gitlab.io/contur-webpage/
        Writing log to contur.log
        INFO - No run point specified, will read all points from DB.
        INFO - Read DB file ANALYSIS/contur_run.db
        ATLAS_13_METJET
        - ATLAS_2017_I1609448
        -- d02-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.46(EXP) 1.00(HLEXP) 
        -- d01-x01-y01 : 0.15(DATABG) 0.15(SMBG) 0.77(EXP) 1.00(HLEXP) 
        -- d04-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 0.25(HLEXP) 
        -- d03-x01-y01 : 0.98(DATABG) 0.98(SMBG) 0.99(EXP) 1.00(HLEXP) 
        CMS_13_L1L2MET
        - CMS_2020_I1814328
        -- d05-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.00(EXP) 0.00(HLEXP) 
        -- d08-x01-y01 : 0.01(DATABG) 0.00(SMBG) 0.00(EXP) 0.00(HLEXP) 
        -- d07-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.00(EXP) 0.00(HLEXP) 
        -- d02-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.00(EXP) 0.00(HLEXP) 
        -- d04-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.00(EXP) 0.00(HLEXP) 
        -- d06-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.00(EXP) 0.00(HLEXP) 
        ...
       
The `-p` (`--print`) flag ensures the info per histogram is only printed to the terminal and no plots are generated yet.
As you can see in the example above, there might be many histograms that do not contribute (much) to the actual exclusion.
It may not be necessary to generate plots for all of these, therefore two options allow you to filter the output by the minimum level of CLs
and the name of the analysis, for example:

        $ contur-rivetplots -p --cls 0.9 --ana-match ATLAS_13
        INFO - Contur version 3.0.x
        INFO - See https://hepcedar.gitlab.io/contur-webpage/
        Writing log to contur.log
        INFO - Read DB file ANALYSIS/contur_run.db
        ATLAS_13_L1L2METJET
          - ATLAS_2019_I1718132:LMODE=ELMU
            -- d55-x01-y01 : 1.00(DATABG) 1.00(SMBG) 0.99(EXP) 1.00(HLEXP) 
            -- d28-x01-y01 : 0.75(DATABG) 0.96(SMBG) 0.70(EXP) 1.00(HLEXP) 
            -- d25-x01-y01 : 0.97(DATABG) 0.99(SMBG) 0.94(EXP) 1.00(HLEXP) 
            -- d52-x01-y01 : 0.77(DATABG) 0.93(SMBG) 0.69(EXP) 1.00(HLEXP) 
            -- d19-x01-y01 : 0.99(DATABG) 1.00(SMBG) 0.99(EXP) 1.00(HLEXP) 
            -- d40-x01-y01 : 0.90(DATABG) 0.98(SMBG) 0.83(EXP) 1.00(HLEXP) 
 
        ...

This command will then only display the histograms using ATLAS 13TeV data and which have a minimum exclusion of 0.9 (90%).

>Hereby, `DATABG` uses the data measured by the analysis in question as background.
This assumes that the analysis did not observe any BSM events and is often necessary if the analysis did not provide a theory prediction as part of their HEPData entry.
`SMBG` uses a proper theory prediction as background.
Note how this can change the exclusion!
`EXP` gives expected exclusion limits as commonly reported by the experiments.
`HLEXP` reports prospective HL-LHC limits.


If you are happy with the filter settings, you can remove the `-p` flag and hit enter. This plots the data, SM prediction and SM+BSM
prediction for the histograms that pass your filter. You can browse these using the generated HTML booklet:

        $ open ANALYSIS/plots/index.html

(Since we don't have a web browser in the docker file, you will need to copy the ANALYSIS directory to a directory which is visible on your machine outside of
docker.)


### Plotting from a grid of results

We'll look at this using the pre-generated grid of results which Contur uses for regression testing.

First, move your old ANALYIS directory out of the way, then copy this grid to your local working directory
```
mv ANALYSIS ANALYSIS-SINGLE
cp -r $CONTUR_DATA_PATH/tests/sources/myscan00 .
```
The command
```
contur -g myscan00 -b 13TeV
```
will run on the small (4x4) test grid for a 13 TeV beam.
You can also change the investigated beam energy to `7TeV` or `8TeV` with the `-b` flag or omit it altogether to consider all beams.

This will produce another ANALYSIS directory with a results database and a plots directory. From the plots directory you can look at the Rivet
histograms like we did above,
by specifying the parameter runpoint, e.g.

```
$ contur-rivetplots --runpoint 13TeV/0010
```

More interestingly, you can now generate a heatmap of exclusions with contur-plot, as follows.

```
contur-plot ANALYSIS/contur_run.db mZp cotH
```

This will make a subdirctory called `conturPlot`, where you can see various images of the 2D heatmap of exclusions. The most instructive are probably
`combinedHybrid.pdf` which shows the grid with a colour bar for exclusions, and the 95\% xpected and actual exclusions; and `dominantPools0.pdf` which shows the
same grid, but now the colouring indicates which final states are the most sensitive at each point.

The black solid (dashed) line thereby corresponds to the exclusion limit at 95% (68%) using the theory prediction.
The dotted line corresponds to the expected limit at 95% confidence level.
It is also possible to estimate the expected 95% HL-LHC sensitivity with the `--hl-estimate` flag, which will be a red dotted line (but might be outside the plotted plane).
With `--secondary-contours` a black solid line corresponding to the exclusion at 95% confidence level using the *measured data as background* is drawn.

The model used for this example is this leptophobic Z-prime model: https://hepcedar.gitlab.io/contur-webpage/results/TopColour/index.html

Which final states are most important? Is that what you'd expect?

### Additional Material

Generating a new grid of results with Contur, either with Madgraph or some other generator (e.g. Herwig) requires the ability to submit jobs to a batch farm, and is beyond
the scope of this tutorial. But you can find help on doing this later here: https://gitlab.com/hepcedar/contur/-/blob/main/doc/READMEs/exclusions2D.md

If you want to try setting up Contur on your own outside docker (for example if you are on a mac with M1 chip, the tutorial docker can be very slow),
see https://gitlab.com/hepcedar/contur/-/blob/main/doc/READMEs/installation.md


