# Rivet Tutorial

Contacts: Christian Gutschow

Welcome to this tutorial, which will lead you through writing your own Rivet routine
and making plots with uncertainty bands using the on-the-fly weights from the MC generators.

This tutorial is optional. It is completely self-guided and you're welcome to give it a go
at any point. Feel free to ask questions as well either in person or via email, in case
you're coming back to this only after the school.


#### Download the docker image for this tutorial `hepstore/contur-tutorial:mcnet24` image. In a terminal, this is accomplished by:

```
docker pull hepstore/contur-tutorial:mcnet24
```
 
#### Run the container interactively, mounting your working directory.

```
docker container run -it -v $PWD:$PWD -w $PWD hepstore/contur-tutorial:mcnet24 /bin/bash
```

where the `-v $PWD:$PWD -w $PWD` ensures that you have access to your current directory also from within the container.
To check that it works, try e.g.

```
  rivet --version
```

which should return `rivet v4.0.0`.


### Known plotting issues

The default `matplotlib` version in the Docker image is a little old and the `rivet-mkhtml` commands
from the tutorial might not work, complaining that
```
Legend.__init__() got an unexpected keyword argument 'alignment'
```
If you encounter this issue, consider updating the `matplotlib`
and `numpy` versions in the image e.g. like so:
```
pip install --root-user-action=ignore --user matplotlib --upgrade
pip install --root-user-action=ignore --user numpy --upgrade
export PYTHONPATH=/root/.local/lib/python3.10/site-packages:$PYTHONPATH
```
Note that these changes are not persistent, so every time you exit and re-enter the image, you would
need to re-upgrade them.

