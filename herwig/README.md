# Herwig Tutorial - MCnet Monte-Carlo School @ CERN 2024
Contact: [Stefan Kiebacher](mailto:stefan.kiebacher@kit.edu?subject=Herwig%20is%20the%20best%20Monte%20Carlo%20event%20generator)

Welcome to the Herwig Tutorial! Here, we will look at the variety of simulation options provided in the Herwig Event Generator. In particular, we will organise our session as:

- Session 1: Setup + First Run
- Session 2: Parton Showers
- Session 3: (N)LO Matching with MC@NLO (POWHEG)
- Session 4: Merging
- If time: Minimum bias

## Session 1: Setup + First Run

For this tutorial you can access Herwig as a [docker container](https://www.docker.com/resources/what-container/). This container will include all the required tools - mainly Herwig and Rivet

### Getting the docker container:

**NOTE for LINUX USERS:** All docker commands may need a `sudo` in front to work.\
**NOTE for MAC USERS:** If you have a M1 chip you may be experiencing a much slower run time

Just execute `docker pull herwigcollaboration/herwig-7.3:7.3.0` to download the Herwig docker image.

### Using the docker container:

Either use `DRH` after using the alias `source env.sh` or run the follwing to get a shell `./run_container.sh` (might need to run `chmod +x run_container.sh` before to give permissions). We will denote further commands requiring the docker image with the prefix `[DRH]`, which just means that you will either need to prefix `DRH` or not if you are using the shell inside the container using `./run_container.sh` \
**Important NOTE:** If you are using `DRH` you may not be able stop the container, without killing docker itself. \
**Less Important NOTE:** If you are using `DRH` you will not enjoy autocompletion for `[DRH] rivet` and `[DRH] Herwig`, whereas with `./run_container.sh` you can use autocompletion.

### Running Herwig

In Herwig, we assign our required process and settings in an input, or a `.in` file. To run Herwig using a `.in` file, we do the following commands:

```bash
[DRH] Herwig read my-process.in # generates my-input-file.run and grid files in Herwig-cache/*
[DRH] Herwig run my-process.run -N <N_EVENTS>
```
The `[DRH] Herwig read` step is generating the matrix elements for the relevant hard process and integrating the hard process over the phase space and building integration grid files (one can separate these two steps also if desired by matrix element generation `[DRH] Herwig build` and integration `[DRH] Herwig integrate`).\
The `[DRH] Herwig run` step is starting the generation of events. This should output a `.log`, `.tex` and a `.yoda` file, amongst others. The `.log` file tells you more about the first few events. The `.tex` file you can compile with `pdflatex LHC-Matchbox.tex` to get the relevant references that you must cite in case you are using `Herwig`. The `.yoda` file allows you to plot your simulation results using `rivet` if you included a `rivet` analysis in your input file:

```bash
[DRH] rivet-mkhtml my-process.yoda
```
`rivet` will produce a directory (`rivet-plots` as default) with an `html` page, which you can view in your favourite browser by e.g. `firefox rivet-plots/index.html`.

For this session and the next, we provide a pre-written input file, `LHC-Matchbox.in`. This contains the required settings to run LHC events for $pp\rightarrow Z + X$ at $7$ TeV at Lowest Order(LO). The showers and matching options are all neatly wrapped in our **Matchbox** framework. An example analysis, `MC_ZINC` has been included already. Have a look at the input file to see what options are availible (commented lines are denoted by starting with `#`).

Start off by running `LHC-Matchbox.in` in it's current state, i.e. doing `read` and `run`, and make the plots with `rivet`. 

**Questions:**

* Why is in the input file written `p p -> e+ e-`?

* Why is the $p_T$ of the $Z$ boson 0?

* Why is the $y_Z$ distribution of the $Z$ boson so broad?

* Why is the $\phi_Z$ distribution of the $Z$ boson only filled at $0$ and $\pi$?

* Looking at the plots and the input file is the parton shower on or off?

For a more in-depth user guide of Herwig please consult the [Herwig Tutorial](https://herwig.hepforge.org/tutorials/index.html), which - albeit not being completely up to date - gives more information and has a search function.

## Session 2: Parton Showers

After a hard subprocess has occured, the external quark and gluon legs radiate quarks and gluons recursively leading to a jet. This cascade of quarks and gluons is called the **Parton Shower**. Herwig offers two parton shower models: the Angular Ordered (AO) (`DefaultShower`) and the Dipole Shower (DS) (`DipoleShower`). Both have their own strengths and weaknesses (see lectures and feel free to ask about it!).

Looking at results from the current state of `LHC-Matchbox.in` you will see that there is no $Z$ $p_T$, why not? You should also have a look at the `.log` file that's outputted and understand the first few events.

Before we activate our shower, let's rename our results from the previous run, or else it will be overwritten:

```bash
mv LHC-Matchbox.yoda LHC-noShower.yoda
```

Now to activate the Shower: Scroll down to the "Matching and Shower Selection" section. Here, you will be able to choose between no shower, the angular ordered shower and the dipole shower:

```bash
##################################################
## Matching and shower selection
## Please also see flavour scheme settings
## towards the end of the input file.
##################################################

#read Matchbox/MCatNLO-DefaultShower.in
# read Matchbox/Powheg-DefaultShower.in
## use for strict LO/NLO comparisons
# read Matchbox/MCatLO-DefaultShower.in
## use for improved LO showering
#read Matchbox/LO-DefaultShower.in ## --> Angular Ordered Shower

# read Matchbox/MCatNLO-DipoleShower.in
# read Matchbox/Powheg-DipoleShower.in
## use for strict LO/NLO comparisons
# read Matchbox/MCatLO-DipoleShower.in
## use for improved LO showering
# read Matchbox/LO-DipoleShower.in ##  --> Dipole Shower

# read Matchbox/NLO-NoShower.in
read Matchbox/LO-NoShower.in ## --> No Shower
```

Uncomment **only one option at a time** and Herwig read + run again. Make sure to change the name of the yoda files as you go along! Any changes to the Z pT? Can you explain why?

**Important Note Showers:** If you want to switch on only the parton shower you should comment the line `set EventHandler:CascadeHandler:MPIHandler NULL`, which turns the MPIs off (don't forget to uncomment it again if you want to use no shower).
**Important Note for Dipole Shower:** If you want to switch to the Dipole Shower you'll need to change the mass scheme to `FiveFlavourNoBMassScheme` in the input file which you can do by replacing:
```
read Matchbox/FiveFlavourScheme.in
## required for dipole shower and fixed order in five flavour scheme
# read Matchbox/FiveFlavourNoBMassScheme.in
read Matchbox/MMHT2014.in
```
with:
```
# read Matchbox/FiveFlavourScheme.in
## required for dipole shower and fixed order in five flavour scheme
read Matchbox/FiveFlavourNoBMassScheme.in
read Matchbox/MMHT2014.in
```
As a useful tool for comparison, you can `rivet-mkhtml` all files in your directory (the `--err` option plots also the statistical error bars):

```bash
rivet-mkhtml --err *.yoda
```

## Session 3: Matching with Madgraph+Openloops and MC@NLO:

The parton shower is designed to work with a leading order process, which means that for NLO matrix elements, the first emission of the parton shower must be changed to _match_ the matrix element. There are different types of matching, amongst those:

- Multiplicative Matching (POWHEG)
- Subtractive Matching (MC@NLO)

The file `LHC-Matchbox.in` is designed to allow you to select the combination of Matching and Shower Scheme you want. However, it takes a while to prepare the NLO integration grids for simulations. To avoid waiting for this during the tutorial, we provide pre-calculated grids for Matching the Angular Ordered Shower with MC@NLO. The matrix element is calculated using `MadGraph` as an amplitude provider and `Openloops` as a loop provider.

To use these grid files, use the following in the read (should take around 5 minutes with the grid files) and run step:

```bash
[DRH] Herwig read LHC-Matchbox-NLO.in -c PreComputedGridFiles/
```

**(and don't rename the .in or .run file!)** After the reading, you need to pass the precompiled grid files to Herwig: 

```bash
[DRH] Herwig run LHC-Matchbox-NLO.run -c PreComputedGridFiles/ -N 10000
```
However you can rename your yoda files if you wish so. Note however that if you change something significant in the input file (e.g. a $p_T$ cut, the centre of mass energy or relevant masses) and read again, and use the pre-computed grids, you will not get the same results since the grids generated may be in a different region of phase space. 

**Questions:**

* What do you observe comparing NLO Matching compared to the LO with parton shower?

## Session 4: Merging:

Merging involves improving the resolution scale of the jets, where a matrix element generate partons above a scale, and a shower generates partons below.

As above, we provide precalculated Merging Grids for Merging + Dipole Shower. To run the `NLO` Merging of one additional jet:

```bash
[DRH] Herwig read LHC-Merging.in -c PreComputedGridFiles/
[DRH] Herwig run LHC-Merging.run -c PreComputedGridFiles/ -N 10000
```
If you would like to Merge with two additional jets you can do this by replacing:
```
do MergingFactory:Process p p -> e+ e- [ j ]
set MergingFactory:NLOProcesses 1
```
with:
```
do MergingFactory:Process p p -> e+ e- [ j j ]
set MergingFactory:NLOProcesses 2
```
However this will take a very long time (as we don't have prepared grids for those)
Now compare the multiple results you have made. What is the difference in Matching and Merging? How does it compare to just Leading order showers?

## All Done? Fancy some Minimum Bias?

Thank you for attending the Herwig Tutorial! If you still have time, feel free to play around with the settings in `LHC-Matchbox.in`. Currently decays, MPI and hadronisation are off - try to turn them on and see what you observe.\
If you want you can also do a minimum bias run, use the provided `LHC-MB.in` file. Since minimum bias has no integration step to perform, the read step is very fast. Make a run and look at the results. Now try turning off the colour reconnection and make a run to see the observables sensitive to colour reconnection.
Turning off the colour reconnection can be done by:
```
cd /Herwig/Hadronization
set ColourReconnector:ColourReconnection No # Turn off ColourReconnection
```
You can also try to make a purely diffraction run, by uncommenting the following lines:
```
cd /Herwig/UnderlyingEvent
set MPIHandler:DiffractiveRatio 1.0 # Only Diffraction
```
Look at the differences and why would you expect these differences?
Aditionally, try using other external NLO matrix element libraries like VBFNLO (note that these may take time to prepare!)

***
_Stefan Kiebacher_ and _Siddharth Sule_ from the _Herwig Collaboration_, February 2024
