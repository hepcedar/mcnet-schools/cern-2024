# navigate to the directory where your RivetMY_W_ANALYIS.so AND MY_W_ANALYIS.yoda file is and run
# "source env.sh" to set the rivet paths below

export RIVET_DATA_PATH=`pwd`
export RIVET_ANALYSIS_PATH=`pwd`
alias DRH='sudo docker run -it --rm -u `id -u $USER`:`id -g` -v $PWD:$PWD -w $PWD herwigcollaboration/herwig-7.3:7.3.0'
