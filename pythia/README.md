# Pythia Tutorial

## MONDAY

**Contact: Christian Bierlich <christian.bierlich@fysik.lu.se>**

Welcome to the PYTHIA tutorial!

In this brief introduction to PYTHIA, you will learn how to obtain, compile, and run PYTHIA. Once you are through the introduction, you can decide whether to explore on your own or learn how to run PYTHIA together with RIVET to make use of the impressive number of analyses implemented there.

You are advised to at least scan through an entire section at a time before starting to type in code so that you know what you are getting into.

:information_source: Don't forget that you can always ask us for help -- but we have tried to provide resources for self-help in the text.

Have a lot of fun!

### Introduction
:stopwatch: Time estimate: 60 minutes.

There is no Docker image intended for the introduction -- it is, in
fact, probably easier to download and run PYTHIA stand-alone, than
to get a Docker installation working. Here is a step-by-step:

1. Make a directory for this tutorial on your computer, and
enter it. Having things contained will make your life easier:

```
mkdir mcnet2024 && cd mcnet2024/
```

2. Go to the PYTHIA webpage [pythia.org](https://www.pythia.org/). Read through the front
page until you find the PYTHIA 8.3 worksheet. Download that, and
follow the instructions in there for installation.

:warning: Make sure that the worksheet is for PYTHIA version 8.312. The filename should be
`worksheet8312.pdf`, and it should be last updated Feb. 2024.

> **_NOTE:_** The installation instructions assume that you are on a Mac or
> Linux computer, with a reasonably modern `C++` compiler installed.
> If this is not the case, you can download a Docker image anyway and
> perform the installation using the tools provided in the container. 
> You will anyway need to use this container image for RIVET later, 
> so it is not a total waste to just download it now.
> 
> a) Download the `hepstore/rivet:latest` image. In a terminal, this is
> accomplished by:
> ```
> docker pull hepstore/rivet:latest
> ```
> 
> b) Run the container interactively, mounting your tutorial directory as the 
> current directory (you are in the tutorial directory as directed, right?):
> ```
> docker container run -it -v $PWD:$PWD -w $PWD hepstore/rivet:latest /bin/bash
> ```
> c) Proceed with the instructions given in the worksheet.

2. Once you have installed PYTHIA, proceed by following chapter 3 and 4
("Hello World!" and "A first realistic analysis").

3. You can now select whether to link to RIVET and try that, or to 
go explore physics cases more on your own!

### Using PYTHIA with RIVET
:stopwatch: Time estimate: 30 minutes.

You have selected to use RIVET -- very nice! 

The first thing you need for RIVET, is to have it installed. 
Since it can be a bit complicated to install RIVET, we will use a container. 

:information_source: To download and run the container, follow steps a) and b) given in the 
introduction, if you skipped those before.

:warning: You should **_NOT_** pull the `hepstore/rivet-pythia:latest` image. That one
already contains a PYTHIA installation, and you want to do the installation yourself.

From inside the interactive container, go to the top PYTHIA directory.
Here you should run:
```
make distclean
```
to ensure that you will do a full recompilation in the container.
Now, configure PYTHIA to be built, linking to RIVET. It is as simple as:
```
./configure --with-rivet # Make sure to inspect the output
make -jN # Where `N` is the number of cores you want to use for compilation
```
Go to the `examples/` directory, and compile `main144`, which is set up
to use RIVET.

:information_source: Bonus information for the experts: `main144` is formerly known as `main93`.

```
cd examples/
make main144
```
This example main can be used together with "command cards" or "run cards",
such as the one seen in `main144.cmnd`. You can run it by issuing:
```
./main144 -c main144.cmnd
```
which results in a histogram file `Rivet.yoda` being produced. You can plot
this by issuing:
```
rivet-mkhtml Rivet.yoda
```
and inspect the plots by opening the resulting `rivet-plots/index.html` file
in a web-brower. 

:warning: There is no web-browser installed in the container, so you need to open the folder from outside the container. If you followed the instructions to mount the current directory inside the container, you can simply open a new terminal tab, navigate to the `mcnet2024` directory, and find the folder there.

You have now produced your first RIVET figures -- congratulations! Here 
are a couple of exercises you could continue with:

1) Instead of doing 7 TeV, change to 13 TeV, and run again for suitable 
13 TeV minimum bias analyses. Find them either on the Rivet webpage at 
`rivet.hepforge.org`, or use this suggestion:
```
Main:analyses = ATLAS_2016_I1419652
```
You want to add more statistics. How do you increase the number of events?
Hint: try to execute `./main144 --help` and read the help text.

2) Try to run an analysis for electron-positron collisions, with suitable 
RIVET analyses. To figure out which settings to use, either go to the
PYTHIA html manual, and click "Getting started/Examples by keywords" and
find suitable examples to copy/paste from, or go to `mcplots.cern.ch`, 
find the analysis you want to run, and copy the run-card from there. 
You can again find suitable analysis suggestion on the RIVET webpage.

Once you are done, you can go exploring on your own!

### Exploring on your own
:stopwatch: Time estimate: however much you like.

You have opted to explore a bit on you own -- excellent choice!

There are many resources for you to learn a bit more about PYTHIA. Here
you get a couple of pointers, which you can use to look at your favourite
physics case.

1. The worksheet includes several more advanced use cases after the two
first examples -- try them out!

2. The main resource for hands-on examples, is the large repository of
example main programs in the `examples/` directory. To get a better
overview, go to the PYTHIA html manual, and click 
"Getting started/Examples by keywords" to browse through examples sorted
by keywords. Find one you like, run it, and try to understand the output.

3. The PYTHIA manual "A comprehensive guide to the physics and usage of 
PYTHIA 8.3", which is linked from the homepage, contains a chapter 3 with
many example settings. Browse through it, find something you find interesting,
and attempt to run it!

4. If you feel like further exploration, the gitlab page `https://gitlab.com/Pythia8/tutorials`
contains a number of advanced mini-tutorials for special topics.
