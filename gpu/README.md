# GPU Tutorial

## TUESDAY & WEDNESDAY

Contact: Enrico Bothmann, Siddharth Sule

The instructions can be found in the worksheet (gpu.pdf) above.

This tutorial presupposes that you have access to a machine that has a recent installation of the Nvidia CUDA toolkit and is equipped with a CUDA-enabled GPU. In case you are a CERN user, you can use lxplus-gpu for this tutorial. This can be accessed by replacing `lxplus` with `lxplus-gpu` when hopping there via `ssh`.
