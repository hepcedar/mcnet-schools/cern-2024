# Tutorials for the MCnet Summer School at CERN (2024)

[WEBSITE: MCNET SUMMER SCHOOL](https://indico.cern.ch/event/1374994/)

## Schedule

### MONDAY
* Getting started
* Generator tutorials
  * [Herwig7](herwig)
  * [Pythia8](pythia)
  * [Sherpa](sherpa)

### TUESDAY & WEDNESDAY
* [Computing on GPUs](gpu)
* [Madgraph+Rivet+Contur](madgraph)



## Prerequisites

Most tutorials will be based on Docker.
If you are using MacOS or Windows, you will first need to create a DockerID at https://hub.docker.com/signup

Head to https://docs.docker.com/install for installation instructions.

You can check that Docker has installed properly by running the `hello-world` Docker image

        $ docker run hello-world

Some helpful commands:

`docker run [OPTIONS] IMAGE` to run an image;
`docker ps` to list active containers;
`docker image ls` to list available images/apps;
`docker attach [OPTIONS] CONTAINER` to attach to a container

When you are running inside a container, you can use `CTLR-p CTLR-q` to detach from it and leave it running.

Please see [tutorial-specific instructions](project#prerequisites) as well.


### Root privileges for Linux users

In case you're being asked for `sudo` privileges when trying to run Docker,
please see these [post-installation notes](https://docs.docker.com/engine/install/linux-postinstall/).

### Missing editor?

The default editor in the images is `nano`.
Feel free to install your favourite editor inside the image, e.g. using

```
apt-get update && apt-get install vim
```

(or emacs, ...)


### Setting up ROOT

In the latest images, the ROOT setup script isn't being sourced properly, resulting in the following error message:

```
ERROR: must "cd where/root/is" before calling ". bin/thisroot.sh" for this version of "qemu-x86_64"!
```

This can be safely ignored as ROOT is not needed for the tutorials.
If you still want to use ROOT, you can set it up like so:
```
cd /usr/local/root/
source bin/thisroot.sh
cd -
```


